const CELL_SIZE = 10;
const SHAPE = 'circle'; // currently supported: circle, square
const COLOR = 'random'; // currently supported: HEX, random
const RANDSEED = true; // random initial population
const SEED = [
    [1, 1, 1, 0, 0, 0, 0, 0, 0],
    [1, 0, 1, 0, 1, 0, 1, 1, 0],
    [1, 0, 1, 0, 1, 0, 0, 1, 0],
    [1, 1, 1, 1, 0, 0, 1, 0, 0],
    [1, 1, 1, 0, 0, 1, 1, 0, 0],
    [0, 1, 1, 1, 0, 0, 0, 1, 1],
    [1, 0, 1, 1, 1, 0, 1, 0, 0],
    [1, 1, 1, 0, 0, 1, 0, 0, 0]
];

var canvas = document.getElementById('gameCanvas');
var canvasWidth = document.body.clientWidth;
var canvasHeight = document.body.clientHeight;

var gridWidth = Math.floor(canvasWidth/CELL_SIZE);
var gridHeight = Math.floor(canvasHeight/CELL_SIZE);

canvas.width = canvasWidth;
canvas.height = canvasHeight;

var ctx = canvas.getContext('2d');
var grid = createArray(gridHeight);

if(RANDSEED) {
    randomPopulate();
} else {
    populateGrid(SEED);
}

drawCanvas();
console.log(grid);
tick();

function tick() {
    drawCanvas();
    updateGrid();
    setTimeout(function() {
        requestAnimationFrame(tick);
    }, 25);
}

function createArray(rows) {
    var arr = [];
    for(var i = 0; i < rows; i++) {
        arr[i] = [];
    }

    return arr;
}

function populateGrid(seed) {
    var seedRow = 0;

    for(var i = 0; i < gridHeight; i++) {
        for(var j = 0; j < gridWidth; j++) {
            grid[i][j] = 0;
            if(seedRow < seed.length && i >= Math.floor((gridHeight - seed.length)/2) &&
                j === Math.floor((gridWidth - seed[0].length)/2)) {
                grid[i].splice(j, seed[0].length, ...seed[seedRow++]);
                j += seed[0].length - 1;
            }
        }
    }
}

function randomPopulate() {
    for(var i = 0; i < gridHeight; i++) {
        for(var j = 0; j < gridWidth; j++) {
            grid[i][j] = Math.round(Math.random());
        }
    }
}

function updateGrid() {
    var tempGrid = createArray(gridHeight);

    for(var i = 1; i < gridHeight - 1; i++) {
        for(var j = 1; j < gridWidth - 1; j++) {
            var neighbors = countLiveNeighbors(i, j);

            switch(neighbors) {
                case 2:
                    tempGrid[i][j] = grid[i][j];
                    break;
                case 3:
                    tempGrid[i][j] = 1;
                    break;
                default:
                    tempGrid[i][j] = 0;
            }
        }
    }

    grid = tempGrid;
}

function countLiveNeighbors(x, y) {
    var count =
        grid[x - 1][y - 1] +
        grid[x][y - 1] +
        grid[x + 1][y - 1] +
        grid[x - 1][y] +
        grid[x + 1][y] +
        grid[x - 1][y + 1] +
        grid[x][y + 1] +
        grid[x + 1][y + 1];

    return count;
}

function drawCanvas() {
    ctx.clearRect(0, 0, canvasWidth, canvasHeight);

    for(var i = 0; i < gridHeight; i++) {
        for(var j = 0; j < gridWidth; j++) {
            if(grid[i][j] === 1) {
                switch(COLOR.toLowerCase()) {
                    case 'random':
                        ctx.fillStyle = rndColor();
                        break;
                    default:
                        ctx.fillStyle = COLOR;
                }

                switch(SHAPE.toLowerCase()) {
                    case 'circle':
                        ctx.beginPath();
                        ctx.arc(j*canvasHeight/gridHeight, i*canvasWidth/gridWidth, 
                            Math.floor(CELL_SIZE/2), 0, 2*Math.PI);
                        ctx.fill();
                        break;
                    case 'square':
                        ctx.fillRect(j*canvasHeight/gridHeight, i*canvasWidth/gridWidth, CELL_SIZE, CELL_SIZE);
                        break;
                    default:
                        console.log('Specify valid shape');
                }
            }
        }
    }
}

function rndColor() {
    var r = Math.floor(255*Math.random()),
        g = Math.floor(255*Math.random()),
        b = Math.floor(255*Math.random());
    return 'rgb(' + r + ',' + g + ',' + b + ')';
}